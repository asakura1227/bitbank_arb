from peewee import *
"""
データベース格納用モデルのクラスです
"""

db_path = "D:\\PycharmProject\\bitbank_arb\\dbdata\\db.db"
import os
if os.path.exists(db_path):
    os.remove(db_path)
db = SqliteDatabase(db_path)

class ticker(Model):

    # id
    id = IntegerField(null=False,primary_key=True)
    # ticker ???/BTC
    other_btc_ticker = FloatField(null=False)
    # ticker ???/JPY
    other_jpy_ticker = FloatField(null=False)
    # ticker BTC/JPY
    btc_jpy_ticker = FloatField(null=False)
    # ticker differ
    btc_other_differ = FloatField(null=False)
    # ticker real ohther_jpy
    real_other_jpy_ticker = FloatField(null=False)
    # inser_time
    insert_time = DateTimeField(null=False)

    class Meta:
        database = db

class depth(Model):

    # id
    id = IntegerField(null=False,primary_key=True)
    # ask,bid
    # other_jpy_ask
    other_jpy_ask = FloatField(null=False)
    # other_jpy_bid
    other_jpy_bid = FloatField(null=False)
    # bcc_btc_ask
    other_btc_ask = FloatField(null=False)
    # bcc_btc_bid
    other_btc_bid = FloatField(null=False)
    # btc_jpy_ask
    btc_jpy_ask = FloatField(null=False)
    # btc_jpy_bid
    btc_jpy_bid = FloatField(null=False)
    # vol
    # other_jpy_ask_vol
    other_jpy_ask_vol = FloatField(null=False)
    # other_jpy_bid_vol
    other_jpy_bid_vol = FloatField(null=False)
    # bcc_btc_ask_vol
    other_btc_ask_vol = FloatField(null=False)
    # bcc_btc_bid_vol
    other_btc_bid_vol = FloatField(null=False)
    # btc_jpy_ask_vol
    btc_jpy_ask_vol = FloatField(null=False)
    # btc_jpy_bid_vol
    btc_jpy_bid_vol = FloatField(null=False)
    # _other_jpy
    depth_other_jpy = TextField(null=False)
    # depth_other_btc
    depth_other_btc = TextField(null=False)
    # depth_btc_jpy
    depth_btc_jpy = TextField(null=False)
    # insert_time
    insert_time = DateTimeField(null=False)

    class Meta:
        database = db

# 取引情報格納用テーブル
# --格納テーブルの内容を考える--
class arb_trading(Model):

    # id
    id = IntegerField(null=False,primary_key=True)
    # other/jpy 合成
    other_jpy_relative = FloatField(null=False)
    # other/jpy 合成
    other_jpy_relative_vol = FloatField(null=False)
    # other/jpy
    other_jpy_real = FloatField(null=False)
    # other/jpy
    other_jpy_real_vol = FloatField(null=False)
    # arb_differ
    profit = FloatField(null=False)
    # insert_time
    insert_time = DateTimeField(null=False)
    class Meta:
        database = db

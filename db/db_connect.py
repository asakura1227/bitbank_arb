#encoding:utf-8
import peewee
from db.db_models import *
"""
Ticker格納用DBと通信するクラスです
"""

# テーブルを作成、存在する場合は作成しない
db.create_tables([ticker,depth,arb_trading])
class db_connect():

    def add_ticker(self,param):

        import datetime
        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        try:
            param["insert_time"] = now_time
            with db.transaction():
                ticker.insert_many(param).execute()
                db.commit()
                return True
        except IntegrityError as ex:
            print(ex)
            db.rollback()
            return False

    def add_depth(self,param):

        import datetime
        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        try:
            param["insert_time"] = now_time
            with db.transaction():
                depth.insert_many(param).execute()
                db.commit()
                return True
        except IntegrityError as ex:
            print(ex)
            db.rollback()
            return False

    def add_arb_trading(self,param):

        import datetime
        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        try:
            param["insert_time"] = now_time
            with db.transaction():
                arb_trading.insert_many(param).execute()
                db.commit()
                return True
        except IntegrityError as ex:
            print(ex)
            db.rollback()
            return False

    def update_ticker(self,param):

            try:
                with db.transaction():
                    ticker_data = ticker.get(ticker.id == param["id"])
                    ticker_data.other_btc_ticker = param["other_btc_ticker"]
                    ticker_data.btc_jpy_tikcer = param["btc_jpy_tikcer"]
                    ticker_data.btc_jpy_tikcer = param["btc_jpy_tikcer"]
                    ticker_data.real_other_jpy_ticker = param["real_other_jpy_ticker"]
                    ticker_data.save()
                    db.commit()
                    return True
            except IntegrityError as ex:
                db.rollback()
                return False

    def get_ticker(self,id):

        try:
            with db.transaction():
                result = ticker.select().where(ticker.id == id)
                if len(result) == 0 or result is None:
                    return None
                else:
                    return result[0]
        except IntegrityError as ex:
            db.rollback()
            return None

    def get_depth(self,id):

        try:
            with db.transaction():
                result = depth.select().where(depth.id == id)
                if len(result) == 0 or result is None:
                    return None
                else:
                    return result[0]
        except IntegrityError as ex:
            db.rollback()
            return None


# coding:utf-8
import python_bitbankcc
from pubnub.enums import PNStatusCategory
from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub, SubscribeListener
import json

# PubNubの設定
pnconfig = PNConfiguration()
pnconfig.subscribe_key = "sub-c-e12e9174-dd60-11e6-806b-02ee2ddab7fe"
pnconfig.ssl = False

class MySubscribeListener(SubscribeListener):

    def var(self):
        self.bcc_jpy_ask = 0
        self.last_bid = 0

    def message(self, pubnub, message):
        # ここに受信したときの処理を記述
        data = message.message

# 受信するチャンネル
CHANNEL_LIST = ['depth_bcc_jpy','depth_bcc_btc']
pubnub = PubNub(pnconfig)
mySubscribeListener = MySubscribeListener()
mySubscribeListener.var()
pubnub.add_listener(mySubscribeListener)
pubnub.subscribe().channels(CHANNEL_LIST).execute()
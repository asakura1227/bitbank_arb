# coding:utf-8


from pubnub.enums import PNStatusCategory
from db.db_connect import *
from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub, SubscribeListener
import json

# PubNubの設定
pnconfig = PNConfiguration()
pnconfig.subscribe_key = "sub-c-e12e9174-dd60-11e6-806b-02ee2ddab7fe"
pnconfig.ssl = False

# BCH,BTC.JPYで取引所アーブ
# コールバック関数の設定
class MySubscribeListener(SubscribeListener):

    def vars(self):
        self.data_id = 1
        self.bcc_btc_ticker = 0
        self.bcc_jpy_ticker = 0
        self.btc_jpy_ticker = 0
        self.db_con = db_connect()

    def message(self, pubnub, message):
        # ここに受信したときの処理を記述

        data = message.message
        if message.channel == "ticker_bcc_btc":
            print("ticker_bcc_btc:" + str(message.message))
            self.bcc_btc_ticker = message.message["data"]["last"]
        elif message.channel == "ticker_bcc_jpy":
            print("ticker_bcc_jpy:" + str(message.message))
            self.bcc_jpy_ticker = message.message["data"]["last"]
        elif message.channel == "ticker_btc_jpy":
            print("ticker_btc_jpy:"+ str(message.message))
            self.btc_jpy_ticker = message.message["data"]["last"]

        if self.bcc_btc_ticker != 0 and self.bcc_jpy_ticker != 0 and self.btc_jpy_ticker != 0:
            param = {
                "id": self.data_id,
                "other_btc_ticker": float(self.bcc_btc_ticker),
                "other_jpy_ticker": float(self.bcc_jpy_ticker),
                "btc_jpy_ticker": float(self.btc_jpy_ticker),
                "btc_other_differ": float(self.bcc_btc_ticker) * float(self.btc_jpy_ticker),
                "real_other_jpy_ticker": float(self.bcc_btc_ticker) * float(self.btc_jpy_ticker) - float(self.bcc_jpy_ticker),
            }
            print("Inser_Data")
            print(param)
            self.db_con.add_ticker(param)
            self.data_id += 1
            # 全tickerを初期化
            self.btc_jpy_ticker = 0
            self.bcc_btc_ticker = 0
            self.bcc_jpy_ticker = 0

# 受信するチャンネル
CHANNEL_LIST = ['ticker_bcc_btc','ticker_bcc_jpy','ticker_btc_jpy']
pubnub = PubNub(pnconfig)
mySubscribeListener = MySubscribeListener()
mySubscribeListener.vars()
pubnub.add_listener(mySubscribeListener)
pubnub.subscribe().channels(CHANNEL_LIST).execute()
# coding:utf-8
from db.db_connect import *
from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub, SubscribeListener
from bitbank_order import *
import const
import datetime

# PubNubの設定
pnconfig = PNConfiguration()
pnconfig.subscribe_key = "sub-c-e12e9174-dd60-11e6-806b-02ee2ddab7fe"
pnconfig.ssl = False

# BCH,BTC.JPYで取引所三角裁定
# コールバック関数の設定
class MySubscribeListener(SubscribeListener):

    # データがそろっているか確認
    def data_check(self):

        # bcc_jpy
        if self.bcc_jpy_ask == 0 or self.bcc_jpy_ask_vol == 0 or self.bcc_jpy_bid ==0 or self.bcc_jpy_bid_vol == 0:
            return False
        # bcc_btc
        if self.bcc_btc_ask == 0 or self.bcc_btc_ask_vol == 0 or self.bcc_btc_bid == 0 or self.bcc_btc_bid_vol == 0:
            return False
        # btc_jpy
        if self.btc_jpy_ask == 0 or self.btc_jpy_ask_vol == 0 or self.btc_jpy_bid == 0 or self.btc_jpy_bid_vol == 0:
            return False
        return True

    def _vars(self):
        # ask,bid
        self.btc_jpy_ask = 0
        self.btc_jpy_bid = 0
        self.bcc_jpy_ask = 0
        self.bcc_jpy_bid = 0
        self.bcc_btc_ask = 0
        self.bcc_btc_bid = 0
        #vol
        self.btc_jpy_ask_vol = 0
        self.btc_jpy_bid_vol = 0
        self.bcc_jpy_ask_vol = 0
        self.bcc_jpy_bid_vol = 0
        self.bcc_btc_ask_vol = 0
        self.bcc_btc_bid_vol = 0
        # data
        self.bcc_jpy_depth = None
        self.bcc_btc_depth = None
        self.btc_jpy_depth = None

    def base_vars(self):
        self.data_id = 1
        self.depth_id = 1
        self.trading_id = 1
        #db
        self.db_con = db_connect()
        self.differ_limit = const.diff_lower # これ以上の価格差の時にトレード
        self.order = bitbank_triangle_arb()

    def ticker_vars(self):

        # ticker
        self.bcc_btc_ticker = 0
        self.bcc_jpy_ticker = 0
        self.btc_jpy_ticker = 0

    def init_vars(self):
        self._vars()
        self.ticker_vars()
        self.base_vars()
        self.order_time = int(datetime.datetime.now().timestamp())
        self.order_time_limit = 5

    # トレードを行うメソッド
    def trading(self,):
        # 指定時間以上たっていない場合は注文を行わない
        if int(datetime.datetime.now().timestamp()) - self.order_time < self.order_time_limit :
            return False
        # 差分を計算
        btc_jpy_ticker = float(self.btc_jpy_ticker)
        arb_diff = float(self.bcc_btc_ticker) * float(self.btc_jpy_ticker) - float(self.bcc_jpy_ticker)
        profit = 0
        if arb_diff > self.differ_limit or arb_diff < - self.differ_limit:
            # btcのロットを計算
            btc_lot = const.btc_lot
            bcc_lot = self.order.cal_lot(self.bcc_btc_ticker, btc_lot)
            # スプレッドを計算してスプレッド総量より差分が大きい場合にトレードを実行する
            # 最新の板情報を取得する
            depth = self.db_con.get_depth(self.depth_id-1)
            # bcc_jpy 買 →　bcc_btc　売り
            if arb_diff > self.differ_limit and depth.other_btc_ask_vol > bcc_lot and depth.other_jpy_bid_vol > bcc_lot:
                profit = depth.other_btc_ask * depth.btc_jpy_bid - depth.other_jpy_bid
                other_jpy_relative = depth.other_btc_ask * btc_jpy_ticker
                other_jpy_real = depth.other_jpy_bid
                other_jpy_relative_vol = depth.other_btc_ask_vol
                other_jpy_real_vol = depth.other_jpy_bid_vol
                # 注文執行
                self.order.bcc_jpy_low_trading(self.bcc_btc_ticker,self.bcc_jpy_ticker,self.btc_jpy_ticker,btc_lot)
            # bcc_btc 買　→　bcc_jpy 売
            elif arb_diff < - self.differ_limit and depth.other_btc_bid_vol > btc_lot and depth.other_jpy_ask_vol > bcc_lot:
                profit = depth.other_jpy_ask - depth.other_btc_bid * depth.btc_jpy_ask
                other_jpy_relative = depth.other_btc_bid * btc_jpy_ticker
                other_jpy_relative_vol = depth.other_btc_bid_vol
                other_jpy_real = depth.other_jpy_ask
                other_jpy_real_vol = depth.other_jpy_ask_vol
                # 注文執行
                self.order.bcc_jpy_high_trading(self.bcc_btc_ticker,self.bcc_jpy_ticker,self.btc_jpy_ticker,btc_lot)
            if profit != 0:
                # dbに投入
                param = {
                    "id":self.trading_id,
                    "other_jpy_relative":other_jpy_relative,
                    "other_jpy_relative_vol":other_jpy_relative_vol,
                    "other_jpy_real":other_jpy_real,
                    "other_jpy_real_vol":other_jpy_real_vol,
                    "profit":profit,
                }
                self.db_con.add_arb_trading(param)
                self.trading_id += 1
                # 頭悪いからidを+1して管理（^q^)
                return True
        return False
    def message(self, pubnub, message):
        # ここに受信したときの処理を記述

        # ---------------------------------------------------------------------------------------------------
        # ticker取得部分
        # ---------------------------------------------------------------------------------------------------

        data = message.message
        if message.channel == "ticker_bcc_btc":
            print("ticker_bcc_btc:" + str(message.message))
            self.bcc_btc_ticker = float(message.message["data"]["last"])
        elif message.channel == "ticker_bcc_jpy":
            print("ticker_bcc_jpy:" + str(message.message))
            self.bcc_jpy_ticker = float(message.message["data"]["last"])
        elif message.channel == "ticker_btc_jpy":
            print("ticker_btc_jpy:"+ str(message.message))
            self.btc_jpy_ticker = float(message.message["data"]["last"])

        if self.bcc_btc_ticker != 0 and self.bcc_jpy_ticker != 0 and self.btc_jpy_ticker != 0:

            trading_result = self.trading()
            # トレードを行う
            if trading_result:
                print("注文を執行しました。")
                self.order_time = int(datetime.datetime.now().timestamp())

            param = {
                "id": self.data_id,
                "other_btc_ticker": float(self.bcc_btc_ticker),
                "other_jpy_ticker": float(self.bcc_jpy_ticker),
                "btc_jpy_ticker": float(self.btc_jpy_ticker),
                "btc_other_differ": float(self.bcc_btc_ticker) * float(self.btc_jpy_ticker),
                "real_other_jpy_ticker": float(self.bcc_jpy_ticker) - float(self.bcc_btc_ticker) * float(self.btc_jpy_ticker),

            }
            print("Inser_Data")
            print(param)
            self.db_con.add_ticker(param)
            self.data_id += 1
            # 全tickerを初期化
            self.ticker_vars()

        # ---------------------------------------------------------------------------------------------------
        # 取得部分
        # ---------------------------------------------------------------------------------------------------
        if message.channel == "depth_bcc_jpy":
            # bcc_jpy
            self.bcc_jpy_ask = float(float(message.message["data"]["asks"][0][1]))
            self.bcc_jpy_ask_vol = message.message["data"]["asks"][0][1]
            self.bcc_jpy_bid = float(message.message["data"]["bids"][0][0])
            self.bcc_jpy_bid_vol = float(message.message["data"]["bids"][0][1])
            self.bcc_jpy_depth = message.message
        if message.channel == "depth_bcc_btc":
            # bcc_btc
            self.bcc_btc_ask = float(float(message.message["data"]["asks"][0][1]))
            self.bcc_btc_ask_vol = message.message["data"]["asks"][0][1]
            self.bcc_btc_bid = float(message.message["data"]["bids"][0][0])
            self.bcc_btc_bid_vol = float(message.message["data"]["bids"][0][1])
            self.bcc_btc_depth = message.message
        if message.channel == "depth_btc_jpy":
            # btc_jpy
            self.btc_jpy_ask = float(float(message.message["data"]["asks"][0][1]))
            self.btc_jpy_ask_vol = message.message["data"]["asks"][0][1]
            self.btc_jpy_bid = float(message.message["data"]["bids"][0][0])
            self.btc_jpy_bid_vol = float(message.message["data"]["bids"][0][1])
            self.btc_jpy_depth = message.message
        # 全データがそろっている場合DBに格納　－　頭悪い(^q^)
        if self.data_check():
            param = {
                "id":self.depth_id,
                "other_jpy_ask":self.bcc_jpy_ask,
                "other_jpy_ask_vol":self.bcc_jpy_ask_vol,
                "other_jpy_bid":self.bcc_jpy_bid,
                "other_jpy_bid_vol":self.bcc_jpy_bid_vol,
                "other_btc_ask":self.bcc_btc_ask,
                "other_btc_ask_vol":self.bcc_btc_ask_vol,
                "other_btc_bid":self.bcc_btc_bid,
                "other_btc_bid_vol":self.bcc_btc_bid_vol,
                "btc_jpy_ask":self.btc_jpy_ask,
                "btc_jpy_ask_vol":self.btc_jpy_ask_vol,
                "btc_jpy_bid":self.btc_jpy_bid,
                "btc_jpy_bid_vol":self.btc_jpy_bid_vol,
                "depth_other_jpy":self.bcc_jpy_depth,
                "depth_other_btc":self.bcc_btc_depth,
                "depth_btc_jpy":self.btc_jpy_depth
            }
            self.depth_id += 1
            self.db_con.add_depth(param)
            self._vars()

# 受信するチャンネル
CHANNEL_LIST = ['ticker_bcc_btc','ticker_bcc_jpy','ticker_btc_jpy','depth_bcc_jpy','depth_bcc_btc','depth_btc_jpy']
pubnub = PubNub(pnconfig)
mySubscribeListener = MySubscribeListener()
mySubscribeListener.init_vars()
pubnub.add_listener(mySubscribeListener)
pubnub.subscribe().channels(CHANNEL_LIST).execute()
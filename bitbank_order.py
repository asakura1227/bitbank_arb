# encoding:utf-8
# bitbank注文を行うプログラムです。
import python_bitbankcc
import const
import logger
import logging
import threading
import json
import time

logger = logging.getLogger(__name__)

# bitanｋで三角取引を行う
class bitbank_triangle_arb():

    def __init__(self):
        self.prv = python_bitbankcc.private(const.bitbankcc_apikey, const.bitbankcc_secretkey)
        # test
        value = self.prv.get_asset()
        print(json.dumps(value))
        self.is_busy = False
        self.time_sleep = 1

    def cal_lot(self,bcc_btc_ticker,btc_lot):
        # ロットを計算する
        bcc_lot = btc_lot / bcc_btc_ticker
        logger.info("btc_lot:"+str(btc_lot))
        logger.info("bcc_lot:"+str(bcc_lot))
        return bcc_lot

    def order(self,pare,price,lot,direction):
        logger.info("注文発注："+str({"pare":str(pare),"price":str(price),"lot":str(lot),"direction":str(direction)}))
        result = self.prv.order(
            pare,
            price,
            lot,
            direction,
            "market"
        )
        logger.info(result)

    # relative_bcc_jpy > real_btc_jpy の時実行
    def bcc_jpy_low_trading(self,bcc_btc_ticker,bcc_jpy_ticker,btc_jpy_ticker,btc_lot):

        # ロットの基準はBTCとする
        # bcc_btc - buy btc - bcc
        # bcc_jpy - sell bcc - jpy
        # btc_jpy - buy jpy - btc

        logger.info("bcc_jpyが高で注文")
        self.is_busy = True

        try:

            bcc_lot = self.cal_lot(bcc_btc_ticker,btc_lot)
            thread_list = []
            bitbankcc_order_1 =threading.Thread(target=self.order("bcc_btc",bcc_btc_ticker,bcc_lot,"buy"))
            bitbankcc_order_2 = threading.Thread(target=self.order("bcc_jpy",bcc_jpy_ticker,bcc_lot,"sell"))
            bitbankcc_order_3 = threading.Thread(target=self.order("btc_jpy",btc_jpy_ticker,btc_lot,"buy"))
            thread_list.append(bitbankcc_order_1)
            thread_list.append(bitbankcc_order_2)
            thread_list.append(bitbankcc_order_3)

            # マルチスレッド開始
            for thread in thread_list:
                thread.start()

            # 終了待ち
            for thread in thread_list:
                thread.join()
        except Exception as e:
            logger.info(e)
            logger.info("注文執行時にエラー発生")

    # real_bcc_jpy > relative_bcc_jpy の時実行
    def bcc_jpy_high_trading(self,bcc_btc_ticker,bcc_jpy_ticker,btc_jpy_ticker,btc_lot):

        # bcc_btc sell - bcc - btc
        # bcc_jpy buy - jpy - bcc
        # btc_jpy sell - btc - jpy

        logger.info("bcc_jpyが低で注文")

        try:

            bcc_lot = self.cal_lot(bcc_btc_ticker,btc_lot)
            thread_list = []
            bitbankcc_order_1 =threading.Thread(target=self.order("bcc_btc",bcc_btc_ticker,bcc_lot,"sell"))
            bitbankcc_order_2 = threading.Thread(target=self.order("bcc_jpy",bcc_jpy_ticker,bcc_lot,"buy"))
            bitbankcc_order_3 = threading.Thread(target=self.order("btc_jpy",btc_jpy_ticker,btc_lot,"sell"))
            thread_list.append(bitbankcc_order_1)
            thread_list.append(bitbankcc_order_2)
            thread_list.append(bitbankcc_order_3)

            # マルチスレッド開始
            for thread in thread_list:
                thread.start()

            # 終了待ち
            for thread in thread_list:
                thread.join()
        except Exception as e:
            logger.info(e)
            logger.info("注文執行時にエラー発生")


# テスト用
if __name__ == "__main__":

    trading = bitbank_triangle_arb()
    trading.cal_lot(0.089,0.1)